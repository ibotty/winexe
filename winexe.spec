Name:           winexe
Version:        1.5
Release:        1%{?dist}
License:        GPLv3
URL:            https://gitlab.com/ibotty/winexe
Summary:        Remote Windows-command executor
Group:          Applications/System
Source0:        %{name}-%{version}.tar.bz2

BuildRequires:  gcc
BuildRequires:  mingw32-gcc
BuildRequires:  mingw64-gcc
BuildRequires:  libtalloc-devel
BuildRequires:  pkgconfig
BuildRequires:  popt-devel
BuildRequires:  python2-devel
BuildRequires:  samba-devel >= 4.5.0
BuildRequires:  waf

# Samba library sonames are weakly versioned, so to be on the safe side,
# let's declare a versioned minimum dependency for samba4-libs
Requires:       samba-libs >= 4.5.0


%description
Winexe remotely executes commands on Windows systems from
GNU/Linux systems by leveraging components of Samba to communicate.

%prep
%autosetup

%build
pushd source
waf configure build --samba-lib-dirs %{_libdir}/samba
popd

%install
pushd source
install -D -m 755 build/%{name} %{buildroot}%{_bindir}/%{name}
install -D -m 644 %{name}.1 %{buildroot}%{_mandir}/man1/%{name}.1
popd

%files
%doc NEWS README
%license COPYING
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1.*


%changelog
* Wed Feb 10 2021 Tobias Florek <tob@butter.sh> 1.5-1
- set samba-lib-dirs explicitly to work around py2 bug (tob@butter.sh)
- update constants for new samba release (tob@butter.sh)

* Wed Feb 10 2021 Tobias Florek <tob@butter.sh> 1.4-1
- Add private copy of smb_msleep() to replace unexported function
  (ngompa@datto.com)
- Drop include for header that no longer exists (ngompa@datto.com)
- Bump the spec and add changelog entry (ngompa13@gmail.com)
- Revise spec to comply better with the Fedora Packaging Guidelines
  (ngompa13@gmail.com)
- Update waf scripts to fix build on Fedora and CentOS (ngompa13@gmail.com)

* Thu Dec 20 2018 Neal Gompa <ngompa13@gmail.com> 1.3-2
- Update compilation flags to further optimize and generate debuginfo
- Revise spec to comply better with the Fedora Packaging Guidelines

* Tue Jul 10 2018 Tobias Florek <tob@butter.sh> 1.3-1
- use external waf (tob@butter.sh)
- fix release to 0 (tob@butter.sh)

* Tue Jul 10 2018 Tobias Florek <tob@butter.sh> 1.2-1b787d2.1
- new package built with tito
- fix compilation with newer samba (including RHEL 7.3)

* Sun Jun 05 2016 Julio Gonzalez Gil <git@juliogonzalez.es> - 1.1-1b787d2.2
- Support CentOS7 again thanks to a patch from Michel Stowe at
  https://sourceforge.net/u/mstowe/winexe/ci/master/tree

* Sun Jun 05 2016 Julio Gonzalez Gil <git@juliogonzalez.es> - 1.1-1b787d2.1
- Require Samba 4.2 as a minimum.
- Change waf script so it can use the new samba 4.x library names

* Fri May 20 2016 Gardar Thorsteinsson <gardart@gmail.com> 1.1-2
- Patch added to fix new Samba4 library names
- new package built with tito

* Tue Oct 20 2015 Julio Gonzalez Gil <git@juliogonzalez.es> - 1.1-1b787d2
- 1.1b787d2 build from http://sourceforge.net/p/winexe/winexe-waf/ci/b787d2a2c4b1abc3653bad10aec943b8efcd7aab

* Tue Mar 25 2014 Tomas Edwardsson <tommi@tommi.org> 1.1-1
- Removed checkout (tommi@tommi.org)
- Updated changelog (tommi@tommi.org)
- Remove patch, already applied (tommi@tommi.org)
- tito releaser configuration added (tommi@tommi.org)

* Tue Mar 25 2014 Tomas Edwardsson <tommi@tommi.org>
- Removed checkout (tommi@tommi.org)
- Updated changelog (tommi@tommi.org)
- Remove patch, already applied (tommi@tommi.org)
- tito releaser configuration added (tommi@tommi.org)

* Tue Mar 25 2014 Tomas Edwardsson <tommi@tommi.org>
- Remove patch, already applied (tommi@tommi.org)
- tito releaser configuration added (tommi@tommi.org)

* Tue Mar 25 2014 Tomas Edwardsson <tommi@tommi.org> 1.1-0.2.20140208gitb787d2a
- new package built with tito

* Tue Feb 11 2014 Satoshi Matsumoto <kaorimatz@gmail.com> - 1.1-0.2.20140208gitb787d2a
- Fix license to GPLv3+

* Sat Feb 08 2014 Satoshi Matsumoto <kaorimatz@gmail.com> - 1.1-0.1.20140208gitb787d2a
- Initial package
